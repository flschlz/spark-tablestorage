package hanbei.spark.tablestorage.client;

import com.azure.data.tables.TableClient;
import com.azure.data.tables.models.ListEntitiesOptions;
import com.azure.data.tables.models.TableEntity;
import com.google.common.base.Joiner;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class TableStorageClient implements Serializable {

    private final TableClient tableClient;
    private final List<String> select;
    private final int limit;
    private final List<String> filters;

    TableStorageClient(TableClient tableClient, List<String> select, int limit, List<String> filters) {
        this.tableClient = tableClient;
        this.select = select;
        this.limit = limit;
        this.filters = Optional.ofNullable(filters).orElse(List.of());
    }

    public Iterable<Map<String, Object>> listEntities() {
        ListEntitiesOptions listEntitiesOptions = new ListEntitiesOptions();

        if (select != null && !select.isEmpty()) {
            listEntitiesOptions.setSelect(select);
        }

        if (limit > 0) {
            listEntitiesOptions.setTop(limit);
        }

        if (filters != null) {
            String filter = Joiner.on("AND").join(filters);
            listEntitiesOptions.setFilter(filter);
        }

        return tableClient.listEntities(listEntitiesOptions, null, null)
                .mapPage(TableEntity::getProperties);
    }

}
