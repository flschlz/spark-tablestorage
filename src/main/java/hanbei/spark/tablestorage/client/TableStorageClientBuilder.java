package hanbei.spark.tablestorage.client;

import com.azure.core.credential.AzureSasCredential;
import com.azure.data.tables.TableClient;
import com.azure.data.tables.TableClientBuilder;
import com.azure.identity.ClientSecretCredential;
import com.azure.identity.ClientSecretCredentialBuilder;

import javax.validation.ValidationException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static hanbei.spark.tablestorage.datasource.Constants.CLIENT_ID;
import static hanbei.spark.tablestorage.datasource.Constants.CLIENT_SECRET;
import static hanbei.spark.tablestorage.datasource.Constants.CONNECTION_STRING;
import static hanbei.spark.tablestorage.datasource.Constants.ENDPOINT;
import static hanbei.spark.tablestorage.datasource.Constants.SAS_KEY;
import static hanbei.spark.tablestorage.datasource.Constants.TABLE;
import static hanbei.spark.tablestorage.datasource.Constants.TENANT_ID;

public class TableStorageClientBuilder implements Serializable {
    private final Map<String, String> config;
    private List<String> select = List.of();

    private int limit = -1;
    private List<String> filters = List.of();

    public TableStorageClientBuilder(Map<String, String> config) {
        this.config = config;
    }

    public TableStorageClientBuilder select(String[] select) {
        this.select = Arrays.asList(select);
        return this;
    }

    public TableStorageClientBuilder limit(int limit) {
        this.limit = limit;
        return this;
    }

    public TableStorageClientBuilder filter(List<String> filters) {
        this.filters = filters;
        return this;
    }

    public TableStorageClient build() {
        if (configContains(CONNECTION_STRING, TABLE)) {
            return createFromConnectionString(
                    config.get(CONNECTION_STRING),
                    config.get(TABLE)
            );
        } else if (configContains(ENDPOINT, SAS_KEY, TABLE)) {
            return createFromSasKey(
                    config.get(ENDPOINT),
                    config.get(SAS_KEY),
                    config.get(TABLE)
            );
        } else if (configContains(ENDPOINT, TABLE, CLIENT_ID, CLIENT_SECRET, TENANT_ID)) {
            return createFromClientCredentials(
                    config.get(ENDPOINT),
                    config.get(TABLE),
                    config.get(CLIENT_ID),
                    config.get(CLIENT_SECRET),
                    config.get(TENANT_ID)
            );
        }
        throw new ValidationException("No valid configuration set");
    }

    private boolean configContains(String... properties) {
        return config.keySet().containsAll(List.of(properties));
    }

    private TableStorageClient createFromSasKey(String endpoint, String sasKey, String table) {
        TableClient tableClient = new TableClientBuilder()
                .endpoint(endpoint)
                .credential(new AzureSasCredential(sasKey))
                .tableName(table)
                .buildClient();
        return new TableStorageClient(tableClient, select, limit, filters);
    }

    private TableStorageClient createFromConnectionString(String connectionString, String table) {
        TableClient tableClient = new TableClientBuilder().connectionString(connectionString).tableName(table).buildClient();
        return new TableStorageClient(tableClient, select, limit, filters);
    }

    private TableStorageClient createFromClientCredentials(String endpoint, String table, String clientId, String secret, String tenantId) {
        ClientSecretCredential credentials = new ClientSecretCredentialBuilder()
                .clientId(clientId)
                .clientSecret(secret)
                .tenantId(tenantId)
                .build();

        TableClient tableClient = new TableClientBuilder()
                .endpoint(endpoint)
                .credential(credentials)
                .tableName(table)
                .buildClient();
        return new TableStorageClient(tableClient, select, limit, filters);
    }
}
