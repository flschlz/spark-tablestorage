package hanbei.spark.tablestorage.client;

import hanbei.spark.tablestorage.Fields;
import org.apache.spark.sql.types.*;

import java.util.Map;
import java.util.Optional;

import static java.util.function.Predicate.not;

public class SchemaInferencer {

    private final TableStorageClient tableStorageClient;
    private StructType schema = new StructType()
            .add(new StructField(Fields.PartitionKey, DataTypes.StringType, false, Metadata.empty()))
            .add(new StructField(Fields.RowKey, DataTypes.StringType, false, Metadata.empty()))
            .add(new StructField(Fields.Timestamp, DataTypes.TimestampType, false, Metadata.empty()));

    public SchemaInferencer(TableStorageClient tableStorageClient) {
        this.tableStorageClient = tableStorageClient;
    }

    public StructType inferSchema() {
        Iterable<Map<String, Object>> tableEntities = tableStorageClient.listEntities();
        for (Map<String, Object> properties : tableEntities) {
            properties.keySet()
                    .stream()
                    .filter(not(Fields::isReserveredProperty))
                    .map(name ->
                            new StructField(name, mapDataType(properties.get(name)), true, Metadata.empty())
                    )
                    .filter(not(schema::contains))
                    .forEach(this::addToSchema);
        }


        return this.schema;
    }

    private DataType mapDataType(Object data) {
        return Optional.ofNullable(data)
                .map(d -> d.getClass().getSimpleName())
                .map(this::switchType)
                .orElse(DataTypes.StringType);
    }

    private DataType switchType(String d) {
        switch (d) {
            case "String":
                return DataTypes.StringType;
            case "Boolean":
                return DataTypes.BooleanType;
            case "Integer":
                return DataTypes.IntegerType;
            case "Short":
                return DataTypes.ShortType;
            case "Long":
                return DataTypes.LongType;
            case "Double":
                return DataTypes.DoubleType;
            case "Float":
                return DataTypes.FloatType;
            case "OffsetDateTime":
                return DataTypes.TimestampType;
            case "byte[]":
                return DataTypes.BinaryType;
            default:
                return DataTypes.StringType;
        }
    }

    private void addToSchema(StructField field) {
        schema = schema.add(field);
    }

}
