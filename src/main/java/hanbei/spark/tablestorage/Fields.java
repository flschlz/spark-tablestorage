package hanbei.spark.tablestorage;


import com.google.common.collect.Lists;

import java.util.List;

public class Fields {
    public static final String RowKey = "RowKey";
    public static final String ODataType = "odata.type";
    public static final String TimestampODataType = "Timestamp@odata.type";
    public static final String ODataId = "odata.id";
    public static final String ODataEtag = "odata.etag";
    public static final String ODataEditLink = "odata.editLink";
    public static final String PartitionKey = "PartitionKey";
    public static final String Timestamp = "Timestamp";

    public static final List<String> excludedFields = Lists.newArrayList(
            TimestampODataType,
            ODataEditLink,
            ODataEtag,
            ODataId,
            ODataType);

    public static final List<String> allFields = Lists.newArrayList(
            RowKey,
            PartitionKey,
            Timestamp,
            TimestampODataType,
            ODataEditLink,
            ODataEtag,
            ODataId,
            ODataType);

    public static boolean isReserveredProperty(String name) {
        return Fields.allFields.contains(name) || name.endsWith("@odata.type");
    }
}