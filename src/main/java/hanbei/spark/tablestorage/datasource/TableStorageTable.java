package hanbei.spark.tablestorage.datasource;

import hanbei.spark.tablestorage.client.TableStorageClientBuilder;
import hanbei.spark.tablestorage.datasource.read.TableStorageScan;
import org.apache.spark.sql.connector.catalog.SupportsRead;
import org.apache.spark.sql.connector.catalog.TableCapability;
import org.apache.spark.sql.connector.read.ScanBuilder;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.sql.util.CaseInsensitiveStringMap;

import java.util.Set;


public class TableStorageTable implements SupportsRead {

    private final StructType schema;
    private final Set<TableCapability> capabilities = Set.of(TableCapability.BATCH_READ);

    public TableStorageTable(StructType schema) {
        this.schema = schema;
    }

    @Override
    public ScanBuilder newScanBuilder(CaseInsensitiveStringMap config) {
        return new TableStorageScan(schema, new TableStorageClientBuilder(config.asCaseSensitiveMap()));
    }

    @Override
    public String name() {
        return "AzureTableStorage";
    }

    @Override
    public StructType schema() {
        return schema;
    }

    @Override
    public Set<TableCapability> capabilities() {
        return capabilities;
    }
}