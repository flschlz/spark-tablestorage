package hanbei.spark.tablestorage.datasource;

public class Constants {

    public static final String ENDPOINT = "endpoint";
    public static final String TABLE = "table";
    public static final String CONNECTION_STRING = "connectionString";
    public static final String INFER_SCHEMA = "inferSchema";
    public static final String SAS_KEY = "sasKey";
    public static final String CLIENT_ID = "clientId";
    public static final String CLIENT_SECRET = "clientSecret";
    public static final String TENANT_ID = "tenantId";
}
