package hanbei.spark.tablestorage.datasource.read;


import hanbei.spark.tablestorage.client.TableStorageClientBuilder;
import org.apache.spark.sql.connector.expressions.filter.Predicate;
import org.apache.spark.sql.connector.read.Batch;
import org.apache.spark.sql.connector.read.Scan;
import org.apache.spark.sql.connector.read.ScanBuilder;
import org.apache.spark.sql.connector.read.SupportsPushDownLimit;
import org.apache.spark.sql.connector.read.SupportsPushDownRequiredColumns;
import org.apache.spark.sql.connector.read.SupportsPushDownV2Filters;
import org.apache.spark.sql.types.StructType;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class TableStorageScan implements Scan, ScanBuilder, SupportsPushDownRequiredColumns, SupportsPushDownLimit, SupportsPushDownV2Filters {

    private final StructType schema;
    private final TableStorageClientBuilder factory;
    private final PredicateConverter predicateConverter;
    private StructType requiredSchema;
    private int limit;
    private final List<Predicate> pushedPredicates = new LinkedList<>();
    private final List<String> filters = new ArrayList<>();

    public TableStorageScan(StructType schema, TableStorageClientBuilder factory) {
        this.schema = schema;
        this.factory = factory;
        this.predicateConverter = new PredicateConverter();
    }

    @Override
    public StructType readSchema() {
        return requiredSchema != null ? requiredSchema : schema;
    }

    @Override
    public String description() {
        return "Azure Table Storage Scanner";
    }

    @Override
    public Batch toBatch() {
        return new TableStorageBatch(readSchema(), factory.limit(limit).select(readSchema().fieldNames()).filter(filters));
    }

    @Override
    public Scan build() {
        return this;
    }

    @Override
    public void pruneColumns(StructType requiredSchema) {
        this.requiredSchema = requiredSchema;
    }

    @Override
    public boolean pushLimit(int limit) {
        this.limit = limit;
        return true;
    }

    @Override
    public Predicate[] pushPredicates(Predicate[] predicates) {
        List<Predicate> notPushedPredicates = new ArrayList<>();
        for (Predicate predicate : predicates) {
            Optional<String> convert = predicateConverter.convert(predicate);
            recordPredicatePushState(predicate, convert, notPushedPredicates);
        }
        return notPushedPredicates.toArray(new Predicate[0]);
    }

    private void recordPredicatePushState(Predicate predicate, Optional<String> convert, List<Predicate> notPushedPredicates) {
        if (convert.isPresent()) {
            pushedPredicates.add(predicate);
            filters.add(convert.get());
        } else {
            notPushedPredicates.add(predicate);
        }
    }

    @Override
    public Predicate[] pushedPredicates() {
        return pushedPredicates.toArray(new Predicate[0]);
    }
}