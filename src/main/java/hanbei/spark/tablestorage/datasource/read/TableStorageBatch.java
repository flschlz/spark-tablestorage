package hanbei.spark.tablestorage.datasource.read;

import hanbei.spark.tablestorage.client.TableStorageClientBuilder;
import org.apache.spark.sql.connector.read.Batch;
import org.apache.spark.sql.connector.read.InputPartition;
import org.apache.spark.sql.connector.read.PartitionReaderFactory;
import org.apache.spark.sql.types.StructType;

public class TableStorageBatch implements Batch {
    private final StructType schema;
    private final TableStorageClientBuilder factory;

    public TableStorageBatch(StructType schema, TableStorageClientBuilder factory) {
        this.schema = schema;
        this.factory = factory;
    }

    @Override
    public InputPartition[] planInputPartitions() {
        return new InputPartition[]{new TableStorageInputPartition()};
    }

    @Override
    public PartitionReaderFactory createReaderFactory() {
        return new TableStoragePartitionReaderFactory(schema, factory);
    }
}
