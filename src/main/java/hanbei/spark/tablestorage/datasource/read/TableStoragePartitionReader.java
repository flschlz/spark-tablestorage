package hanbei.spark.tablestorage.datasource.read;


import hanbei.spark.tablestorage.Fields;
import hanbei.spark.tablestorage.client.TableStorageClientBuilder;
import org.apache.spark.sql.catalyst.InternalRow;
import org.apache.spark.sql.connector.read.PartitionReader;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import scala.collection.JavaConverters;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public class TableStoragePartitionReader implements PartitionReader<InternalRow> {
    private final StructType schema;
    private Iterator<Map<String, Object>> entities = null;
    private final TableStorageClientBuilder factory;

    public TableStoragePartitionReader(StructType schema, TableStorageClientBuilder factory) {
        this.schema = schema;
        this.factory = factory;
    }

    @Override
    public boolean next() {
        initializeEntitites();
        return entities.hasNext();
    }

    private void initializeEntitites() {
        if (entities == null) {
            this.entities = factory.build().listEntities().iterator();
        }
    }

    @Override
    public InternalRow get() {
        initializeEntitites();

        Map<String, Object> properties = entities.next();

        List<Object> resultList = new ArrayList<>();
        for (StructField field : schema.fields()) {
            if (Fields.excludedFields.contains(field.name())) continue;

            Object value = properties.get(field.name());
            Function converter = ValueConverters.getConverter(field.dataType());
            Object convertedValue = converter.apply(value);
            resultList.add(convertedValue);
        }

        Iterator<Object> resultIterator = resultList.iterator();
        return InternalRow.apply(JavaConverters.asScalaIteratorConverter(resultIterator).asScala().toSeq());
    }

    @Override
    public void close() {
        // no need to close
    }

}