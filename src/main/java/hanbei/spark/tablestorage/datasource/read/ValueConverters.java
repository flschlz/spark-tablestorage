package hanbei.spark.tablestorage.datasource.read;


import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.unsafe.types.UTF8String;

import java.time.OffsetDateTime;
import java.util.function.Function;

public class ValueConverters {

    public static Function getConverter(DataType dataType) {
        if (dataType.equals(DataTypes.StringType)) {
            return UTF8StringConverter;
        } else if (dataType.equals(DataTypes.TimestampType)) {
            return TimestampConverter;
        }
        return IdentityConverter;
    }

    private static final Function IdentityConverter = value -> value;
    private static final Function<String, UTF8String> UTF8StringConverter = UTF8String::fromString;
    private static final Function<OffsetDateTime, Long> TimestampConverter =
            value -> value == null ? null : value.toInstant().toEpochMilli() * 1000L;

}