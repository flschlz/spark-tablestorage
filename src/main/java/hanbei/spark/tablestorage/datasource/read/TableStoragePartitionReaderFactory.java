package hanbei.spark.tablestorage.datasource.read;

import hanbei.spark.tablestorage.client.TableStorageClientBuilder;
import org.apache.spark.sql.catalyst.InternalRow;
import org.apache.spark.sql.connector.read.InputPartition;
import org.apache.spark.sql.connector.read.PartitionReader;
import org.apache.spark.sql.connector.read.PartitionReaderFactory;
import org.apache.spark.sql.types.StructType;

public class TableStoragePartitionReaderFactory implements PartitionReaderFactory {
    private final StructType schema;
    private final TableStorageClientBuilder clientFactory;

    public TableStoragePartitionReaderFactory(StructType schema, TableStorageClientBuilder factory) {
        this.schema = schema;
        this.clientFactory = factory;
    }

    @Override
    public PartitionReader<InternalRow> createReader(InputPartition inputPartition) {
        return new TableStoragePartitionReader(schema, clientFactory);
    }

}