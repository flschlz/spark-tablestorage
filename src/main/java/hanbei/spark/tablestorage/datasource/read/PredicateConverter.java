package hanbei.spark.tablestorage.datasource.read;

import org.apache.spark.sql.connector.expressions.Expression;
import org.apache.spark.sql.connector.expressions.LiteralValue;
import org.apache.spark.sql.connector.expressions.NamedReference;
import org.apache.spark.sql.connector.expressions.filter.Predicate;
import org.apache.spark.sql.types.TimestampNTZType;
import org.apache.spark.sql.types.TimestampType;

import java.util.Optional;

public class PredicateConverter {
    public Optional<String> convert(Predicate predicate) {
        String result = null;
        switch (predicate.name()) {
            case "=":
                result = convertExpression(predicate.children()[0]) + " eq " + convertExpression(predicate.children()[1]);
                break;
            case "!=":
            case "<>":
                result = convertExpression(predicate.children()[0]) + " ne " + convertExpression(predicate.children()[1]);
                break;
            case ">":
                result = convertExpression(predicate.children()[0]) + " gt " + convertExpression(predicate.children()[1]);
                break;
            case "<":
                result = convertExpression(predicate.children()[0]) + " lt " + convertExpression(predicate.children()[1]);
                break;
            case ">=":
                result = convertExpression(predicate.children()[0]) + " ge " + convertExpression(predicate.children()[1]);
                break;
            case "<=":
                result = convertExpression(predicate.children()[0]) + " le " + convertExpression(predicate.children()[1]);
                break;
        }
        return Optional.ofNullable(result);
    }

    private String convertExpression(Expression e) {
        if (e instanceof NamedReference) {
            return ((NamedReference) e).fieldNames()[0];
        } else if (e instanceof LiteralValue) {
            if (isTimeValue((LiteralValue<?>) e)) {
                return "datetime'" + e + "Z'";
            } else {
                return e.toString();
            }
        }
        return null;
    }

    private boolean isTimeValue(LiteralValue<?> e) {
        return e.dataType() instanceof TimestampType || e.dataType() instanceof TimestampNTZType;
    }

}
