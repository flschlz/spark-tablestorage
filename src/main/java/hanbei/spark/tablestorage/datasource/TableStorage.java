package hanbei.spark.tablestorage.datasource;


import hanbei.spark.tablestorage.client.SchemaInferencer;
import hanbei.spark.tablestorage.client.TableStorageClient;
import hanbei.spark.tablestorage.client.TableStorageClientBuilder;
import org.apache.spark.sql.connector.catalog.Table;
import org.apache.spark.sql.connector.catalog.TableProvider;
import org.apache.spark.sql.connector.expressions.Transform;
import org.apache.spark.sql.sources.DataSourceRegister;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.sql.util.CaseInsensitiveStringMap;

import java.util.Map;

public class TableStorage implements TableProvider, DataSourceRegister {

    @Override
    public StructType inferSchema(CaseInsensitiveStringMap config) {
        TableStorageClient tableStorageClient = new TableStorageClientBuilder(config.asCaseSensitiveMap()).limit(100).build();

        SchemaInferencer schemaInferencer = new SchemaInferencer(tableStorageClient);
        return schemaInferencer.inferSchema();
    }

    @Override
    public Table getTable(StructType structType, Transform[] transforms, Map<String, String> config) {
        return new TableStorageTable(structType);
    }

    @Override
    public String shortName() {
        return "ats";
    }
}