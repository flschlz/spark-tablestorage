package hanbei.spark.tablestorage.datasource.read;

import hanbei.spark.tablestorage.Fields;
import hanbei.spark.tablestorage.client.TableStorageClient;
import hanbei.spark.tablestorage.client.TableStorageClientBuilder;
import org.apache.spark.sql.catalyst.InternalRow;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.unsafe.types.UTF8String;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class TableStoragePartitionReaderTest {

    private TableStoragePartitionReader reader;
    private TableStorageClient storageClient;

    @BeforeEach
    void setup() {
        storageClient = mock(TableStorageClient.class);

        TableStorageClientBuilder storageClientFactory = mock(TableStorageClientBuilder.class);
        when(storageClientFactory.build()).thenReturn(storageClient);
        reader = new TableStoragePartitionReader(expectedSchema(), storageClientFactory);
    }

    @Test
    void test_empty_table() {
        when(storageClient.listEntities()).thenReturn(List.of());

        assertThat(reader.next()).isFalse();
    }

    @Test
    void test_one_row() {
        when(storageClient.listEntities()).thenReturn(List.of(entity(1)));

        assertThat(reader.next()).isTrue();

        InternalRow row = reader.get();
        assertRow(row, 1);

        assertThat(reader.next()).isFalse();
    }

    @Test
    void test_multiple_rows() {
        List<Map<String, Object>> entities = List.of(
                entity(0),
                entity(1),
                entity(2),
                entity(3)
        );
        when(storageClient.listEntities()).thenReturn(entities);

        for (int i = 0; i < entities.size(); i++) {
            assertThat(reader.next()).isTrue();
            InternalRow row = reader.get();
            assertRow(row, i);
        }

        assertThat(reader.next()).isFalse();
    }

    @ParameterizedTest
    @MethodSource
    void test_null_values_in_entity(Integer key, String str, Integer intField, Double doubleField) {
        List<Map<String, Object>> entities = List.of(entityWithNullValues(key, str, intField, doubleField));
        when(storageClient.listEntities()).thenReturn(entities);

        assertRowWithNullValues(reader.get(), key, str, intField, doubleField);
    }

    public static Stream<Arguments> test_null_values_in_entity() {
        return Stream.of(
                Arguments.of(0, null, 0, 2.0),
                Arguments.of(1, "asd", null, 2.0),
                Arguments.of(2, "asd", 0, null)
        );
    }

    private void assertRowWithNullValues(InternalRow row, Integer key, String str, Integer i1, Double v) {
        assertThat(row.numFields()).isEqualTo(10);

        assertThat(row.getString(0)).isEqualTo(key.toString());
        assertThat(row.getString(1)).isEqualTo(key.toString());
        assertThat(row.getLong(2)).isEqualTo(1674216672000000L);
        assertThat(row.getBinary(3)).isNull();
        assertThat(row.getDouble(4)).isEqualTo(v != null ? v : 0.0);
        assertThat(row.getFloat(5)).isEqualTo(0.0f);
        assertThat(row.getInt(6)).isEqualTo(i1 != null ? i1 : 0);
        assertThat(row.getLong(7)).isEqualTo(0L);
        assertThat(row.getShort(8)).isEqualTo((short) 0);
        assertThat(row.getUTF8String(9)).isEqualTo(UTF8String.fromString(str));

    }

    private Map<String, Object> entityWithNullValues(Integer key, String str, Integer i1, Double v) {
        TreeMap<String, Object> map = new TreeMap<>();
        map.put("PartitionKey", key.toString());
        map.put("RowKey", key.toString());
        map.put("Timestamp", OffsetDateTime.of(2023, 1, 20, 12, 11, 12, 33, ZoneOffset.UTC));
        map.put("stringField", str);
        map.put("doubleField", v);
        map.put("intField", i1);
        return map;
    }

    private static void assertRow(InternalRow row, Integer key) {
        assertThat(row.numFields()).isEqualTo(10);
        assertThat(row.getString(0)).isEqualTo(key.toString());
        assertThat(row.getString(1)).isEqualTo(key.toString());
        assertThat(row.getLong(2)).isEqualTo(1674216672000000L);
        assertThat(row.getBinary(3)).isEqualTo(new byte[]{0, 1, 2, 3});
        assertThat(row.getDouble(4)).isEqualTo(3.0);
        assertThat(row.getFloat(5)).isEqualTo(4.0f);
        assertThat(row.getInt(6)).isEqualTo(1);
        assertThat(row.getLong(7)).isEqualTo(2L);
        assertThat(row.getShort(8)).isEqualTo((short) 12);
        assertThat(row.getString(9)).isEqualTo("asdasd");
    }

    private Map<String, Object> entity(int key) {
        return new TreeMap<>(Map.of(
                "PartitionKey", "" + key,
                "RowKey", "" + key,
                "Timestamp", OffsetDateTime.of(2023, 1, 20, 12, 11, 12, 33, ZoneOffset.UTC),
                "binaryField", new byte[]{0, 1, 2, 3},
                "intField", 1,
                "longField", 2L,
                "shortField", (short) 12,
                "doubleField", 3.0,
                "floatField", 4.0f,
                "stringField", "asdasd"
        ));
    }

    private StructType expectedSchema() {
        return new StructType()
                .add(new StructField(Fields.PartitionKey, DataTypes.StringType, false, Metadata.empty()))
                .add(new StructField(Fields.RowKey, DataTypes.StringType, false, Metadata.empty()))
                .add(new StructField(Fields.Timestamp, DataTypes.TimestampType, false, Metadata.empty()))
                .add(new StructField("binaryField", DataTypes.BinaryType, true, Metadata.empty()))
                .add(new StructField("doubleField", DataTypes.DoubleType, true, Metadata.empty()))
                .add(new StructField("floatField", DataTypes.FloatType, true, Metadata.empty()))
                .add(new StructField("intField", DataTypes.IntegerType, true, Metadata.empty()))
                .add(new StructField("longField", DataTypes.LongType, true, Metadata.empty()))
                .add(new StructField("shortField", DataTypes.ShortType, true, Metadata.empty()))
                .add(new StructField("stringField", DataTypes.StringType, true, Metadata.empty()));
    }

}