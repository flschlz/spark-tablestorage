package hanbei.spark.tablestorage.datasource.read;

import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.unsafe.types.UTF8String;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class ValueConvertersTest {

    private final static OffsetDateTime dateTime = OffsetDateTime.of(2023, 12, 12, 12, 0, 1, 23, ZoneOffset.UTC);

    public static Stream<Arguments> test_single_type_conversion() {
        return Stream.of(
                Arguments.of(DataTypes.StringType, "input", UTF8String.fromString("input")),
                Arguments.of(DataTypes.IntegerType, 123, 123),
                Arguments.of(DataTypes.LongType, 2431313123132L, 2431313123132L),
                Arguments.of(DataTypes.ShortType, (short) 125, (short) 125),
                Arguments.of(DataTypes.FloatType, 1.5f, 1.5f),
                Arguments.of(DataTypes.DoubleType, 2.5f, 2.5f),
                Arguments.of(DataTypes.BooleanType, true, true),
                Arguments.of(DataTypes.TimestampType, dateTime, 1702382401000000L),
                Arguments.of(DataTypes.BinaryType, new byte[]{1, 2, 3}, new byte[]{1, 2, 3})
        );
    }

    @ParameterizedTest
    @MethodSource
    void test_single_type_conversion(DataType dataType, Object input, Object result) {
        assertThat(ValueConverters.getConverter(dataType).apply(input)).isEqualTo(result);
    }

}