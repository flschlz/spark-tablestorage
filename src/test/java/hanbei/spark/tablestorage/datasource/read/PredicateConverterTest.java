package hanbei.spark.tablestorage.datasource.read;

import org.apache.spark.sql.connector.expressions.Expression;
import org.apache.spark.sql.connector.expressions.filter.Predicate;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.stream.Stream;

import static org.apache.spark.sql.connector.expressions.Expressions.column;
import static org.apache.spark.sql.connector.expressions.Expressions.literal;
import static org.assertj.core.api.Assertions.assertThat;

class PredicateConverterTest {

    private final PredicateConverter predicateConverter = new PredicateConverter();

    @ParameterizedTest
    @MethodSource
    void test_conversion(String operator, String column, Object value, String result) {
        Predicate predicate = new Predicate(operator, new Expression[]{column(column), literal(value)});

        Optional<String> filter = predicateConverter.convert(predicate);

        assertThat(filter).contains(result);
    }


    @Test
    void operator_is_null() {
        Predicate predicate = new Predicate(null, new Expression[]{column("RowKey"), literal(1)});

        Optional<String> filter = predicateConverter.convert(predicate);

        assertThat(filter).contains("");
    }

    public static Stream<Arguments> test_conversion() {
        return Stream.of(
                Arguments.of("=", "RowKey", "1", "RowKey eq '1'"),
                Arguments.of("<>", "RowKey", "1", "RowKey ne '1'"),
                Arguments.of(">", "RowKey", "1", "RowKey gt '1'"),
                Arguments.of(">=", "RowKey", "1", "RowKey ge '1'"),
                Arguments.of("<", "RowKey", "1", "RowKey lt '1'"),
                Arguments.of("<=", "RowKey", "1", "RowKey le '1'"),
                Arguments.of("=", "RowKey", 1, "RowKey eq 1"),
                Arguments.of("!=", "RowKey", 1, "RowKey ne 1"),
                Arguments.of(">", "RowKey", 1, "RowKey gt 1"),
                Arguments.of(">=", "RowKey", 1, "RowKey ge 1"),
                Arguments.of("<", "RowKey", 1, "RowKey lt 1"),
                Arguments.of("<=", "RowKey", 1, "RowKey le 1"),
                Arguments.of("=", "RowKey", LocalDateTime.of(2024, 1, 3, 12, 34, 56), "RowKey eq datetime'2024-01-03T12:34:56Z'"),
                Arguments.of("<>", "RowKey", LocalDateTime.of(2024, 1, 3, 12, 34, 56), "RowKey ne datetime'2024-01-03T12:34:56Z'"),
                Arguments.of(">", "RowKey", LocalDateTime.of(2024, 1, 3, 12, 34, 56), "RowKey gt datetime'2024-01-03T12:34:56Z'"),
                Arguments.of(">=", "RowKey", LocalDateTime.of(2024, 1, 3, 12, 34, 56), "RowKey ge datetime'2024-01-03T12:34:56Z'"),
                Arguments.of("<", "RowKey", LocalDateTime.of(2024, 1, 3, 12, 34, 56), "RowKey lt datetime'2024-01-03T12:34:56Z'"),
                Arguments.of("<=", "RowKey", LocalDateTime.of(2024, 1, 3, 12, 34, 56), "RowKey le datetime'2024-01-03T12:34:56Z'")
        );
    }

}