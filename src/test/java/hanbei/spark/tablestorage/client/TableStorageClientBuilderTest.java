package hanbei.spark.tablestorage.client;

import org.junit.jupiter.api.Test;

import javax.validation.ValidationException;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class TableStorageClientBuilderTest {

    @Test
    void test_table_not_set() {
        TableStorageClientBuilder factory = new TableStorageClientBuilder(Map.of());
        assertThatThrownBy(factory::build).isInstanceOf(ValidationException.class).hasMessageContaining("No valid configuration set");
    }

    @Test
    void test_connection_string_set() {
        TableStorageClientBuilder factory = new TableStorageClientBuilder(Map.of(
                "table", "bla",
                "connectionString", "DefaultEndpointsProtocol=http;AccountName=devstoreaccount1;AccountKey=Eby8vdM02xNOcqFlqUwJPLlmEtlCDXJ1OUzFT50uSRZ6IFsuFq2UVErCz4I6tq"
        ));

        assertThat(factory.build()).isNotNull();
    }

}