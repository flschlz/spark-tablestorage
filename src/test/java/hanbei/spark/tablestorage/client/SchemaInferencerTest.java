package hanbei.spark.tablestorage.client;

import hanbei.spark.tablestorage.Fields;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class SchemaInferencerTest {

    private SchemaInferencer schemaInferencer;

    @BeforeEach
    void setup() {
        TableStorageClient tableStorageClient = mock(TableStorageClient.class);
        when(tableStorageClient.listEntities()).thenReturn(List.of(entity(1), entity(2), entity(3)));
        schemaInferencer = new SchemaInferencer(tableStorageClient);
    }

    private StructType expectedSchema() {
        return new StructType()
                .add(new StructField(Fields.PartitionKey, DataTypes.StringType, false, Metadata.empty()))
                .add(new StructField(Fields.RowKey, DataTypes.StringType, false, Metadata.empty()))
                .add(new StructField(Fields.Timestamp, DataTypes.TimestampType, false, Metadata.empty()))
                .add(new StructField("binaryField", DataTypes.BinaryType, true, Metadata.empty()))
                .add(new StructField("doubleField", DataTypes.DoubleType, true, Metadata.empty()))
                .add(new StructField("floatField", DataTypes.FloatType, true, Metadata.empty()))
                .add(new StructField("intField", DataTypes.IntegerType, true, Metadata.empty()))
                .add(new StructField("longField", DataTypes.LongType, true, Metadata.empty()))
                .add(new StructField("shortField", DataTypes.ShortType, true, Metadata.empty()))
                .add(new StructField("stringField", DataTypes.StringType, true, Metadata.empty()));
    }

    private Map<String, Object> entity(int key) {
        return new TreeMap<>(Map.of(
                "PartitionKey", "" + key,
                "RowKey", "" + key,
                "Timestamp", OffsetDateTime.of(2023, key, 20, 12, 11, 12, 33, ZoneOffset.UTC),
                "binaryField", new byte[]{0, 1, 2, 3},
                "intField", 1,
                "longField", 2L,
                "shortField", (short) 12,
                "doubleField", 3.0,
                "floatField", 3.0f,
                "stringField", "asdasd"
        ));
    }

    @Test
    void test_infer_schema() {
        StructType inferredSchema = schemaInferencer.inferSchema();

        assertThat(inferredSchema).isEqualTo(expectedSchema());
    }

}