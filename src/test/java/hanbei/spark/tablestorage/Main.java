package hanbei.spark.tablestorage;

import org.apache.spark.sql.SparkSession;

public class Main {

    public static void main(String[] args) {
        var sparkSession = SparkSession
                .builder()
                .master("local[1]")
                .appName("spark test example")
                // speedups for testing
                .config("spark.sql.shuffle.partitions", "1")
                .config("spark.broadcast.compress", "false")
                .config("spark.shuffle.compress", "false")
                .config("spark.shuffle.spill.compress", "false")
                .config("spark.sql.shuffle.partitions", "1")
                .getOrCreate();

        var rows = sparkSession.read()
                .format("ats")
                .option(
                        "connectionString",
                        "DefaultEndpointsProtocol=http;AccountName=devstoreaccount1;AccountKey=Eby8vdM02xNOcqFlqUwJPLlmEtlCDXJ1OUzFT50uSRZ6IFsuFq2UVErCz4I6tq/K1SZFPTOtr/KBHBeksoGMGw==;TableEndpoint=http://127.0.0.1:10002/devstoreaccount1;")
                .option("table", "testtable")
                .load()
                .select(Fields.PartitionKey, Fields.RowKey, Fields.Timestamp)
                .where("RowKey > '1'")
                .limit(10)
                .collectAsList();

        rows.forEach(System.out::println);
    }
}
